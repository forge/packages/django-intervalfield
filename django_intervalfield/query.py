from django.db.models import Q, F
from .interval import Interval

__all__ = (
    "interval_contains",
    "interval_exact",
    "interval_gte",
    "interval_gt",
    "interval_in",
    "interval_invalid",
    "interval_lte",
    "interval_lt",
    "interval_valid",
)

_FIELD_NAMES = ("start", "start_included", "end", "end_included")


def Q_empty(path):
    return Q(**{f"{path}__isnull": True,}) & Q(
        **{
            f"{path}__isnull": False,
        }
    )


def Q_if(condition, *args, **kwargs):
    return Q(*args, **kwargs) if condition else Q()


def interval_contains(path, value, scalar=True):
    if isinstance(value, Interval):
        if not value:
            raise ValueError("value is not a valid interval")
        return Q(
            interval_valid(path),
            Q(**{f"{path}_start__isnull": True})
            | Q_if(
                value.start is not None,
                **{
                    f"{path}_start__lt": value.start,
                },
            )
            | Q_if(
                value.start is not None,
                Q_if(
                    not value.start_included,
                    **{
                        f"{path}_start_included": False,
                    },
                ),
                **{
                    f"{path}_start__lte": value.start,
                },
            ),
            Q(**{f"{path}_end__isnull": True})
            | Q_if(
                value.end is not None,
                **{
                    f"{path}_end__gt": value.end,
                },
            )
            | Q_if(
                value.end is not None,
                Q_if(
                    not value.end_included,
                    **{
                        f"{path}_end_included": False,
                    },
                ),
                **{
                    f"{path}_end__gte": value.end,
                },
            ),
        )

    elif isinstance(value, F):
        if not scalar:
            return Q(
                interval_valid(path),
                interval_valid(value.name),
                Q(**{f"{path}_start__isnull": True})
                | Q(
                    **{
                        f"{path}_start__lt": F(f"{value.name}_start"),
                    }
                )
                | Q(
                    **{
                        f"{path}_start__lte": F(f"{value.name}_start"),
                        f"{path}_start_included": True,
                    }
                )
                | Q(
                    **{
                        f"{path}_start__lte": F(f"{value.name}_start"),
                        f"{value.name}_start_included": False,
                    }
                ),
                Q(**{f"{path}_end__isnull": True})
                | Q(
                    **{
                        f"{path}_end__lt": F(f"{value.name}_end"),
                    }
                )
                | Q(
                    **{
                        f"{path}_end__gte": F(f"{value.name}_end"),
                        f"{path}_end_included": True,
                    }
                )
                | Q(
                    **{
                        f"{path}_end__gte": F(f"{value.name}_end"),
                        f"{value.name}_end_included": False,
                    }
                ),
            )
    return Q(
        interval_valid(path),
        Q(
            **{
                f"{path}_start__lt": value,
            }
        )
        | Q(
            **{
                f"{path}_start__isnull": True,
            }
        )
        | Q(
            **{
                f"{path}_start__lte": value,
                f"{path}_start_included": True,
            }
        ),
        Q(
            **{
                f"{path}_end__gt": value,
            }
        )
        | Q(
            **{
                f"{path}_end__isnull": True,
            }
        )
        | Q(
            **{
                f"{path}_end__gte": value,
                f"{path}_end_included": True,
            }
        ),
    )


def interval_exact(path, value):
    if isinstance(value, F):
        return Q(
            **{
                f"{path}_{field}__exact": F(f"{value.name}_{field}")
                for field in _FIELD_NAMES
            }
        )
    elif isinstance(value, Interval):
        return Q(
            **{
                f"{path}_{field}__exact": getattr(value, field)
                for field in _FIELD_NAMES
            }
        )
    raise TypeError("value must be an Interval")


def _interval_gt(path, value, op, scalar=True):
    if isinstance(value, F):
        if scalar:
            return Q(
                **{
                    f"{path}_start__{op}": value,
                }
            )
        else:
            return Q(**{f"{path}_start__{op}": F(f"{value.name}_end"),}) | Q(
                Q(
                    **{
                        f"{path}_start_included": False,
                    }
                )
                | Q(
                    **{
                        f"{value.name}_end_included": False,
                    }
                ),
                **{
                    f"{path}_start__gte": F(f"{value.name}_end"),
                },
            )
    if isinstance(value, Interval):
        if scalar:
            if value.end is None:
                return Q_empty(path)
            return Q(
                **{
                    f"{path}__{op}": value.end,
                }
            )
        else:
            if value.end is None:
                return Q_empty(f"{path}_end")
            return Q(**{f"{path}_start__{op}": value.end,}) | Q(
                Q_if(
                    value.end_included,
                    **{
                        f"{path}_start_included": False,
                    },
                ),
                **{
                    f"{path}_start__gte": value.end,
                },
            )
    return Q(**{f"{path}_start__{op}": value}) | Q(
        **{
            f"{path}_start__gte": value,
            f"{path}_start_included": False,
        }
    )


def interval_gt(path, value, scalar=True):
    return _interval_gt(path, value, "gt", scalar=scalar)


def interval_gte(path, value, scalar=True):
    return _interval_gt(path, value, "gte", scalar=scalar)


def interval_in(path, value, scalar=True):
    if isinstance(value, F):
        return interval_contains(value.name, F(path), scalar=scalar)
    if not isinstance(value, Interval) or not value:
        raise TypeError("value must be a valid Interval")
    if scalar:
        return Q(
            Q_if(
                value.start is not None
                and not value.start_included
                ** {
                    f"{path}_gt": value.start,
                }
            )
            | Q_if(
                value.start is not None
                and value.start_included
                ** {
                    f"{path}_gte": value.start,
                }
            ),
            Q_if(
                value.end is not None
                and not value.end_included
                ** {
                    f"{path}_lt": value.end,
                }
            )
            | Q_if(
                value.end is not None
                and value.end_included
                ** {
                    f"{path}_lte": value.end,
                }
            ),
        )
    else:
        return Q(
            interval_valid(path),
            Q_if(
                value.start is not None,
                Q(
                    **{
                        f"{path}_start__gt": value.start,
                    }
                )
                | Q(
                    Q_if(
                        value.start_included,
                        **{
                            f"{path}_start_included": True,
                        },
                    ),
                    **{
                        f"{path}_start__gte": value.start,
                    },
                ),
            ),
            Q_if(
                value.end is not None,
                Q(
                    **{
                        f"{path}_end__lt": value.end,
                    }
                )
                | Q(
                    Q_if(
                        value.end_included,
                        **{
                            f"{path}_end_included": True,
                        },
                    ),
                    **{
                        f"{path}_end__lte": value.end,
                    },
                ),
            ),
        )


def _interval_lt(path, value, op, scalar=True):
    if isinstance(value, F):
        if scalar:
            return Q(
                **{
                    f"{path}_end__{op}": value,
                }
            )
        else:
            return _interval_gt(value.name, F(path), op=op, scalar=scalar)
    if isinstance(value, Interval):
        if scalar:
            if value.start is None:
                return Q_empty(path)
            return Q(
                **{
                    f"{path}__{op}": value.start,
                }
            )
        else:
            if value.start is None:
                return Q_empty(f"{path}_start")
            return Q(**{f"{path}_end__{op}": value.start,}) | Q(
                Q_if(
                    value.start_included,
                    **{
                        f"{path}_end_included": False,
                    },
                ),
                **{
                    f"{path}_end__lte": value.start,
                },
            )
    return Q(**{f"{path}_end__{op}": value}) | Q(
        **{
            f"{path}_end__lte": value,
            f"{path}_end_included": False,
        }
    )


def interval_lt(path, value, scalar=True):
    return _interval_lt(path, value, "lt", scalar=scalar)


def interval_lte(path, value, scalar=True):
    return _interval_lt(path, value, "lte", scalar=scalar)


def interval_invalid(path):
    return ~interval_valid(path)


def interval_valid(path):
    return (
        Q(
            **{
                f"{path}_start__isnull": True,
            }
        )
        | Q(
            **{
                f"{path}_end__isnull": True,
            }
        )
        | Q(
            **{
                f"{path}_start__lt": F(f"{path}_end"),
            }
        )
        | Q(
            **{
                f"{path}_start__exact": F(f"{path}_end"),
                f"{path}_start_included": True,
                f"{path}_end_included": True,
            }
        )
    )
